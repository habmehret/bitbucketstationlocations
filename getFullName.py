def getFullName (f, l):
    if f is None || l is None:
        return None
    full = f + " " + l

    return full

print(getFullName("Hab", "Mehreteab"))